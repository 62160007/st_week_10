<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Change Password_f26a4d6c6f7488c</name>
   <tag></tag>
   <elementGuidId>d1f97a62-9faf-4ee0-a6e0-71a52fbb039b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@name='f26a4d6c6f7488c']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>iframe[name=&quot;f26a4d6c6f7488c&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>f26a4d6c6f7488c</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1000px</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1000px</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>dialog_iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>frameborder</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowtransparency</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>no</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allow</name>
      <type>Main</type>
      <value>encrypted-media</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://www.facebook.com/v12.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3202c07a2c5074%26domain%3Dmyid.buu.ac.th%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fmyid.buu.ac.th%252Ffa222ad14f398%26relation%3Dparent.parent&amp;container_width=979&amp;current_url=https%3A%2F%2Fmyid.buu.ac.th%2Fprofile%2Fchgpwdlogin&amp;is_loaded_by_facade=true&amp;locale=th_TH&amp;log_id=0dfbab5a-2174-4919-a88e-6d73891a750e&amp;page_id=160067777401491&amp;request_time=1646844465212&amp;sdk=joey</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fb-root&quot;)/div[@class=&quot;fb_iframe_widget fb_invisible_flow&quot;]/span[1]/iframe[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@name='f26a4d6c6f7488c']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fb-root']/div[2]/span/iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@name = 'f26a4d6c6f7488c' and @src = 'https://www.facebook.com/v12.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3202c07a2c5074%26domain%3Dmyid.buu.ac.th%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fmyid.buu.ac.th%252Ffa222ad14f398%26relation%3Dparent.parent&amp;container_width=979&amp;current_url=https%3A%2F%2Fmyid.buu.ac.th%2Fprofile%2Fchgpwdlogin&amp;is_loaded_by_facade=true&amp;locale=th_TH&amp;log_id=0dfbab5a-2174-4919-a88e-6d73891a750e&amp;page_id=160067777401491&amp;request_time=1646844465212&amp;sdk=joey']</value>
   </webElementXpaths>
</WebElementEntity>
