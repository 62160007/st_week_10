import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.junit.Assert
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/')

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Username)_user'), '62160007')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Password)_pass'), '9y6aLSUBL53UUQ8x6QmM6g==')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Sign in'))

WebUI.click(findTestObject('Page_My ID/a_Change PasswordRenew password'))

WebUI.setEncryptedText(findTestObject('Page_My ID/input_(New Password)_newpass'), 's8chN608gLMAwltWigxkKg==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Re-New Password)_renewpass'), 's8chN608gLMAwltWigxkKg==')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Change Password'))

actualResultFirst = WebUI.getText(findTestObject('Page_My ID/p_Change password successfully'))
expectedResult = 'เปลี่ยนรหัสผ่านสำเร็จ\n\nChange password successfully.'
Assert.assertEquals(expectedResult, actualResultFirst)

WebUI.click(findTestObject('Page_My ID/a_Change PasswordRenew password'))

WebUI.setEncryptedText(findTestObject('Page_My ID/input_(New Password)_newpass'), '9y6aLSUBL53UUQ8x6QmM6g==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Re-New Password)_renewpass'), '9y6aLSUBL53UUQ8x6QmM6g==')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Change Password'))

actualResultSecond = WebUI.getText(findTestObject('Page_My ID/p_Change password successfully'))
Assert.assertEquals(expectedResult, actualResultSecond)

WebUI.closeBrowser()

