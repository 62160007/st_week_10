<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Myid Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>0985cf59-cff4-4ea2-b04b-de72d849d6a8</testSuiteGuid>
   <testCaseLink>
      <guid>ba8f2a96-fc82-4726-b246-0d88fbc59aaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73d5d2f8-8edd-40c4-9f20-50e7346f0051</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>32d5eb84-c588-42a0-b482-d471266e03c1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4d112afa-1d82-41cc-a939-39120269c196</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ff6b161-2217-42c8-93be-7bc51b484cbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pass Change Fail no char</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f30c87d-7419-45da-b6fe-cd5a84dc5809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pass Change Fail char less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f13e14a-6fce-4042-b329-447d58a2496e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pass Change Fail no num</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a891d616-fe52-4d62-8620-84023f1a2dda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pass Change Fail no special char</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c39a7a5d-d1cf-466c-be4a-dd34f2055c7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pass Change Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
